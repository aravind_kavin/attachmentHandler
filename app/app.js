

(function () {
  "use strict";
  return {
    initialize: function () {
      domHelper.ticket.onReplyClick(this.replyClicked.bind(this));

      if (page_type == "ticket") {
        console.log(domHelper);
        var ticketId = domHelper.ticket.getTicketInfo()
          .helpdesk_ticket.display_id;
        console.log(ticketId)

        var options = {
          headers:
            {
              "Authorization": "Basic dFI1TmwzSEJ4UFk0a1pnMEducXo=",
              "content-type": "application/json"
            }
        }

        var url = "https://spritle12.freshdesk.com/api/v2/tickets/" + ticketId + "/conversations";
        this.$request.get(url, options).done(
          function (data) {
            var dataResponse = JSON.parse(data.response);
            dataResponse.map(function (data, i) {
              console.log(data.attachments.length)
              if (data.attachments.length > 0) {
                var attachments = data.attachments;
                console.log(attachments);
                attachments.map(function (data, i) {
                  var attachmentLogo = "<div id=attachmentBoxLeft class='attachment-box-left col-xs-2'><span id='attachmentLogo' class='attachment-logo glyphicon glyphicon-file' ></span></div>";
                  var attachmentUrl = data.attachment_url;
                  var attachmentFileName = data.name;
                  var attachmentFileSize = Math.floor(data.size / 1024);
                  var attachmentFileSizeText = "(" + attachmentFileSize + " KB)";
                  var attachmentData = '<div id="attachmentBoxRight" class="attachment-box-right col-xs-10">';
                  attachmentData += '<span id="attachmentName" class="attachment-name"><a href= "' + attachmentUrl + '" target="_blank">' + attachmentFileName + '</a></span>';
                  attachmentData += '<br><span id="attachmentSize" class="attachment-size">' + attachmentFileSizeText + '</span>';
                  attachmentData += '</div><br><br><br>';
                  jQuery("#attachmentBox").append(attachmentLogo + attachmentData);
                })
              }
            })
          })
          .fail(function (error) {
            console.log(error);
          }

          )

      }



    },
    replyClicked: function (e) {
      jQuery('#trigger_attachment').append('<br><br><br><button id="verify" style="background-color: #65b158;color: white;padding: 10px 27px; text-align: center;text-decoration: none; display: inline-block; font-size: 18px;border: none; margin-left:10%; -webkit-transition-duration: 0.4s;  " >Verify Attachments</button><br><br><br>');
      jQuery('#verify').on('click', function () {

        var flag=0;
        var data = jQuery('.ftype');
        console.log(data);
        for (var i = 0; i < data.length; i++) {
          var fileType = data[i].innerHTML;
          console.log(fileType);
          if (fileType == '') 
          flag=0;
          if (fileType == '.pdf' || fileType == '.tiff') 
          flag=0;
     
          else {
            flag=1;
            break;
          }
        }
        if(flag==0){
          jQuery('.dropup').show();
          window.alert("Validation Successfull");
        }
        else{
          window.alert("Please attach a valid file");
          jQuery('.dropup').hide();
        }

      })





    }
  }
}

)();